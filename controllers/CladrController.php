<?php

namespace app\controllers;

use Yii;
use app\models\Cladr;
use app\models\CladrSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\web\Response;
use linslin\yii2\curl;

/**
 * CladrController implements the CRUD actions for Cladr model.
 */
class CladrController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cladr models.
     * @return mixed
     */
    public function actionIndex()
    {

        $model = new Cladr();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'name' => $model->name, 'searchString' => $model->searchString, 'regname' => $model->regname]);
        }
        if (!empty($model->pointsent) & !empty($model->pointget)) {
            $model->code_sent = $model->getCityCode($model->pointsent);
            $model->code_get = $model->getCityCode($model->pointget);
        }
        if (empty($model->pointsent) & empty($model->pointget)) {
            $request = Yii::$app->request;
            $model->code_sent = $request->get('from');
            $model->code_get = $request->get('to');
        }
        if (empty($model->send_shipment) & empty($model->get_shipment)){
            $model->send_shipment = true;
            $model->get_shipment = true;
        }
        if (empty($model->type_packaging)){
            $model->type_packaging = '0xad22189d098fb9b84eec0043196370d6';
        }

        $value = [
            'derivalPoint' => $model->code_sent,
            'arrivalPoint' => $model->code_get,
            'length' => $model->shipment_length,
            'width' => $model->shipment_width,
            'height' => $model->shipment_height,
            'count_place' => $model->count_place,
            'weight_one_place' => $model->weight_one_place,
            'packages' => $model->type_packaging,
            'derivalDoor' => $model->send_shipment,
            'arrivalDoor' => $model->get_shipment,
        ];
        $model->value_json = json_encode($value, true);

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Cladr model.
     * @param integer $id
     * @param string $name
     * @param string $searchString
     * @param string $regname
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $name, $searchString, $regname)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $name, $searchString, $regname),
        ]);
    }

    /**
     * Creates a new Cladr model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Cladr();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'name' => $model->name, 'searchString' => $model->searchString, 'regname' => $model->regname]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing Cladr model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $name
     * @param string $searchString
     * @param string $regname
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $name, $searchString, $regname)
    {
        $model = $this->findModel($id, $name, $searchString, $regname);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'name' => $model->name, 'searchString' => $model->searchString, 'regname' => $model->regname]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cladr model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $name
     * @param string $searchString
     * @param string $regname
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $name, $searchString, $regname)
    {
        $this->findModel($id, $name, $searchString, $regname)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cladr model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $name
     * @param string $searchString
     * @param string $regname
     * @return Cladr the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $name, $searchString, $regname)
    {
        if (($model = Cladr::findOne(['id' => $id, 'name' => $name, 'searchString' => $searchString, 'regname' => $regname])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCitylist($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $query = new Query();
            $query->select('name AS id, code AS text')
                ->from('cladr')
                ->where(['like', 'name', $q])
                ->limit(10);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Cladr::findOne($id)->name];
        }

        return $out;
    }

    public function actionCalc()
    {

        $model = new Cladr();

        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;

        if (!empty($request->get())) {
            $result = json_decode($request->get('value', true));
        }

        $sizedWeight = $result->count_place * $result->weight_one_place;
        $sizedVolume = str_replace(',', '.', $result->length) * str_replace(',', '.', $result->width) * str_replace(',', '.', $result->height);
        $params = [
            'appkey' => 'F3B342E4-B256-4008-B8C0-665D8F784714',
            'sessionID' => 'D8C07080-3DD8-47E9-B66C-A1E64BB64CC2',
            'derivalPoint' => $result->derivalPoint,
            'arrivalPoint' => $result->arrivalPoint,
            'derivalDoor' => $result->derivalDoor,
            'arrivalDoor' => $result->arrivalDoor,
            'sizedVolume' => $sizedVolume,
            'sizedWeight' => $sizedWeight,
            'oversizedVolume' => '0',
            'oversizedWeight' => '0',
            'length' => str_replace(',', '.', $result->length),
            'width' => str_replace(',', '.', $result->width),
            'height' => str_replace(',', '.', $result->height),
            //'freight_uid' => 'false',
            'statedValue' => '1000',
            'packages' => $result->packages,
        ];
        $curl = new curl\Curl();

        return $model->res = json_decode($curl->setRequestBody(json_encode($params))
            ->setHeaders([
                'Content-Type' => 'application/json',
                'Content-Length' => strlen(json_encode($params))
            ])
            ->post('https://api.dellin.ru/v1/public/calculator.json'), true);

    }
}
