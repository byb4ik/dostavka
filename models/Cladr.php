<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;


class Cladr extends ActiveRecord
{
    public $code_sent;
    public $code_get;
    public $shipment_height = 0.2;
    public $shipment_width = 0.2;
    public $shipment_length = 1;
    public $pointsent;
    public $pointget;
    public $count_place = 1;
    public $weight_one_place = 1;
    public $type_packaging;
    public $send_shipment;
    public $get_shipment;
    public $value_json;
    public $res;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cladr';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cityID', 'name', 'code', 'searchString', 'regname', 'regcode', 'zonname', 'zoncode'], 'required'],
            [['cityID', 'name', 'code', 'searchString', 'regname', 'regcode', 'zonname', 'zoncode', 'code_sent',
                'code_get', 'shipment_height', 'shipment_width', 'shipment_length', 'pointsent', 'pointget',
                'count_place', 'weight_one_place', 'type_packaging', 'send_shipment', 'get_shipment', 'res', 'value_json'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cityID' => 'City ID',
            'name' => 'Name',
            'code' => 'Code',
            'searchString' => 'Search String',
            'regname' => 'Regname',
            'regcode' => 'Regcode',
            'zonname' => 'Zonname',
            'zoncode' => 'Zoncode',
            'pointsent' => 'Место отправления',
            'pointget' => 'Место назначения',
            'shipment_height' => 'Высота груза',
            'shipment_width' => 'Ширина груза',
            'shipment_length' => 'Длина груза',
            'count_place' => 'Количество мест',
            'weight_one_place' => 'Масса одного места',
            'type_packaging' => 'Тип упаковки',
            'code_sent' => 'Код места отправления',
            'code_get' => 'Код места получения',
            'send_shipment' => 'Забрать у заказчика',
            'get_shipment' => 'Доставить адресату',
        ];
    }

    public function getShipTypeList()
    {
        return [
            '0x838fc70baeb49b564426b45b1d216c15' => 'Жесткая упаковка',
            '0x951783203a254a05473c43733c20fe72' => 'Картонная коробка',
            '0x9a7f11408f4957d7494570820fcf4549' => 'Дополнительная упаковка',
            '0xa8b42ac5ec921a4d43c0b702c3f1c109' => 'Воздушно-пузырьковая плёнка',
            '0xad22189d098fb9b84eec0043196370d6' => 'Мешок',
        ];
    }

    public function getCityCode($cityname)
    {
       return self::findOne(['name' => $cityname])->code;
    }

    public function sendShipDataList()
    {
        return [
            'true' => 'Забрать у заказчика',
            'false' => 'Отправка с терминала'
        ];
    }

    public function getShipDataList()
    {
        return [
            'true' => 'Доставить адресату',
            'false' => 'Доставка до терминала'
        ];
    }

}
