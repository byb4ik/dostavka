<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cladr;

/**
 * CladrSearch represents the model behind the search form of `app\models\Cladr`.
 */
class CladrSearch extends Cladr
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['cityID', 'name', 'code', 'searchString', 'regname', 'regcode', 'zonname', 'zoncode'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cladr::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'cityID', $this->cityID])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'searchString', $this->searchString])
            ->andFilterWhere(['like', 'regname', $this->regname])
            ->andFilterWhere(['like', 'regcode', $this->regcode])
            ->andFilterWhere(['like', 'zonname', $this->zonname])
            ->andFilterWhere(['like', 'zoncode', $this->zoncode]);

        return $dataProvider;
    }
}
