<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cladr */

$this->title = 'Расчет стоимости доставки';
//$this->params['breadcrumbs'][] = ['label' => 'Cladrs', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>


<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
<?php
var_dump($model);
?>

<img src="/img/18306_edb06c582bc768d408e4d2fd57e3404c.jpg" alt="">
<div class="order-index">
    <div class="box box-default">
        <div class="box-body">
            <div data-spy="scroll" data-target="#navbar-example" data-offset="0"
                 style="height:auto;overflow:auto; position: relative; border: padding: 10px;">
                <div id="api">
                    <h4>Отправление из:</h4>
                    <div id="data-derival-city"><img src="/img/loading.gif" alt="" width="200"></div>
                    <h4>Адреса терминалов отправки:</h4>
                    <div id="data-derival-terminals"></div>
                    <h4>Доставка в:</h4>
                    <div id="data-arrrival-city"></div>
                    <h4>Адреса терминалов доставки:</h4>
                    <div id="data-arrival-terminals"></div>
                    <h4>Данные межтерминальной доставки:</h4>
                    <div id="data-price"></div>
                    <strong>Дата отправки из ОСП-отправителя: </strong>
                    <div id="data-order_dates-derrival_from_osp_sender"></div>
                    <strong>Дата прибытия в ОСП-получатель: </strong>
                    <div id="data-order_dates-arrival_to_osp_receiver"></div>
                    <strong>Срок доставки груза: </strong>
                    <div id="data-time-value"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var str = location.href;
    //  var arr = str.split('/');
    //  + arr['5']
    $.ajax({
        url: '/cladr/calc/?from=1000000100000000000000000&to=100000100700000000000000',
        dataType: 'json',
        error: function () {
            $("#api").html('Ошибка, попробуйте еще раз или обратитесь к администратору');
        },
        success: function (data) {
            console.log(data);
            if (data.errors != null) {
                $("#api").html(data.errors.messages[0]);
            }
            $("#data-derival-city").html(data.derival.city);
            $.each(data.derival.terminals, function () {
                $('#data-derival-terminals').append(this.address + '<br>');
            });
            $("#data-arrrival-city").html(data.arrival.city);
            $.each(data.arrival.terminals, function () {
                $('#data-arrival-terminals').append(this.address + '<br>');
            });
            $("#data-price").html(data.price);
            $("#data-air-price").html(data.price);
            $("#data-time-value").html(data.time.nominative);
            $("#data-order_dates-derrival_from_osp_sender").html(data.order_dates.derrival_from_osp_sender);
            $("#data-order_dates-arrival_to_osp_receiver").html(data.order_dates.arrival_to_osp_receiver);
        }
    });
</script>