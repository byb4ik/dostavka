<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cladr */

$this->title = 'Update Cladr: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Cladrs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id, 'name' => $model->name, 'searchString' => $model->searchString, 'regname' => $model->regname]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cladr-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
