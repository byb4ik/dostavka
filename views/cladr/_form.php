<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;

?>

<?php $form = ActiveForm::begin(); ?>

<div class="container">
    <div class="row">

        <div class="col-sm-6">
            <?php
            echo $form->field($model, 'pointsent')->widget(Select2::classname(), [
                'options' => ['placeholder' => 'Поиск города ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Поиск...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::to('/cladr/citylist'),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function (cladr) { return cladr.id; }'),
                    'templateSelection' => new JsExpression('function (cladr) { return cladr.id; }'),
                ],
            ]);

            ?>
        </div>


        <div class="col-sm-6">

            <?php
            echo $form->field($model, 'pointget')->widget(Select2::classname(), [
                'options' => ['placeholder' => 'Поиск города ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Поиск...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::to('/cladr/citylist'),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function (cladr) { return cladr.id; }'),
                    'templateSelection' => new JsExpression('function (cladr) { return cladr.id; }'),
                ],
            ]);

            ?>

        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-4">

            <?= $form->field($model, 'shipment_height')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'shipment_width')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'shipment_length')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'count_place')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'weight_one_place')->textInput(['maxlength' => true]) ?>
        </div>

    </div>
</div>

    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'send_shipment')->dropDownList($model->sendShipDataList()) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'get_shipment')->dropDownList($model->getShipDataList()) ?>
            </div>
        </div>
    </div>

<div class="container">
    <div class="row">
        <div class="col-sm-3">
        <?= $form->field($model, 'type_packaging')->dropDownList($model->getShipTypeList()) ?>
    </div>
    </div>
</div>

<div class="form-group">
    <?= Html::submitButton('Узнать стоимость', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>


<?php
//var_dump($model->res);