<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cladr */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Cladrs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cladr-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id, 'name' => $model->name, 'searchString' => $model->searchString, 'regname' => $model->regname], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id, 'name' => $model->name, 'searchString' => $model->searchString, 'regname' => $model->regname], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cityID',
            'name',
            'code',
            'searchString',
            'regname',
            'regcode',
            'zonname',
            'zoncode',
        ],
    ]) ?>

</div>
